#!/bin/sh
set -e
SRC_FILE="haml-mode.el"
# Right now only emacs23, emacs24 and emacs25 are supported. If the future
# brings support for other emacsen, we can keep this same logic - just
# update this regex
SUPPORTED_EMACSEN="^emacs2[345]$"
FLAVOR="$1"

if echo $FLAVOR | egrep -vq $SUPPORTED_EMACSEN; then
  echo "Skipping byte-compilation for ${FLAVOR}: Not supported"
  exit 0
fi

echo "install/haml-elisp: Handling install of emacsen flavor ${FLAVOR}"

if [ "${FLAVOR}" != "emacs" ]; then
  SRC_FLAVOR_PATH=/usr/share/${FLAVOR}/site-lisp/haml-elisp/$SRC_FILE    
  echo "install/haml-elisp: byte-compiling for ${FLAVOR}"
  cd /usr/share/emacs/site-lisp/haml-elisp
  mkdir -p /usr/share/${FLAVOR}/site-lisp/haml-elisp
  
  echo "Installing source code for ${FLAVOR}"
  # Remove the symlink or file if already exists
  rm -f $SRC_FLAVOR_PATH

  ln -s /usr/share/emacs/site-lisp/haml-elisp/$SRC_FILE \
       /usr/share/${FLAVOR}/site-lisp/haml-elisp/$SRC_FILE

  cd /usr/share/${FLAVOR}/site-lisp/haml-elisp
  cat <<EOF > path.el
(setq load-path (cons "." load-path) byte-compile-warnings nil)
EOF
  FLAVORTEST=`echo $FLAVOR | cut -c-6`
  if [ ${FLAVORTEST} = xemacs ] ; then
    SITEFLAG="-no-site-file"
  else
    SITEFLAG="--no-site-file"
  fi
  ${FLAVOR} -q ${SITEFLAG} -batch -l path.el -f batch-byte-compile $SRC_FILE
  rm path.el 

fi

exit 0;

